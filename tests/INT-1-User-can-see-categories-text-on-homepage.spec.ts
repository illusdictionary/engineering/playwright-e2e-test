import { test, expect } from '@playwright/test';

test('User can see categories text on homepage', async ({ page }) => {
  await page.goto('http://localhost:3000/');

  // Expect home page "to contain" categories text.
  const element = await page.getByText('Categories');
  await expect(element !== undefined ).toBeTruthy()
});
